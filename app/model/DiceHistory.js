const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const diceSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId,
    },
    user: {
        type: mongoose.Types.ObjectId,
        required: true,
        ref: "user"
    },
    dice: {
        type: Number,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    },
})

module.exports = mongoose.model("dice", diceSchema);