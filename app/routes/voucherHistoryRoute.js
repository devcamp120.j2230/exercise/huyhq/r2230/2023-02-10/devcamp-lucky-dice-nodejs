const express = require("express");

const {
    createVoucherHistory,
    getAllVoucherHistory,
    getVoucherHistoryById,
    updateVoucherHistoryById,
    deleteVoucherHistoryById
} = require("../controller/VoucherHistoryController");

const voucherHistoryRouter = express.Router();

voucherHistoryRouter.get("/voucher-history", getAllVoucherHistory);
voucherHistoryRouter.post("/voucher-history", createVoucherHistory);
voucherHistoryRouter.get("/voucher-history/:voucherHistoryId", getVoucherHistoryById);
voucherHistoryRouter.put("/voucher-history/:voucherHistoryId", updateVoucherHistoryById);
voucherHistoryRouter.delete("/voucher-history/:voucherHistoryId", deleteVoucherHistoryById);

module.exports = {voucherHistoryRouter};