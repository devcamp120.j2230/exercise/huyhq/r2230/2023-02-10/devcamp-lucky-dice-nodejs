const express = require("express");

const {
    getLuckyDice
} = require("../controller/luckyDiceController");

const luckDiceRouter = express.Router();

luckDiceRouter.post("/dice", getLuckyDice);


module.exports = {luckDiceRouter};