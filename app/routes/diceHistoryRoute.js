const express = require("express");

const {
    createDiceHistory,
    getAllDiceHistory,
    getDiceHistoryById,
    updateDiceHistoryById,
    deleteDiceHistoryById
} = require("../controller/diceHistoryController");

const diceHistoryRouter = express.Router();

diceHistoryRouter.get("/dice-history", getAllDiceHistory);
diceHistoryRouter.post("/dice-history", createDiceHistory);
diceHistoryRouter.get("/dice-history/:diceId", getDiceHistoryById);
diceHistoryRouter.put("/dice-history/:diceId", updateDiceHistoryById);
diceHistoryRouter.delete("/dice-history/:diceId", deleteDiceHistoryById);

module.exports = {diceHistoryRouter};