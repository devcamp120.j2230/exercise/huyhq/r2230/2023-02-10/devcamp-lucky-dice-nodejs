const express = require("express");

const {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById
} = require("../controller/userController");

const userRouter = express.Router();

userRouter.get("/user", getAllUser);
userRouter.post("/user", createUser);
userRouter.get("/user/:userId", getUserById);
userRouter.put("/user/:userId", updateUserById);
userRouter.delete("/user/:userId", deleteUserById);

module.exports = {userRouter};