const { default: mongoose } = require("mongoose");

const voucherHistoryModel = require("../model/VoucherHistory");
const userModel = require("../model/User");

const createVoucherHistory = (req, res) => {
    let body = req.body;

    if (!body.user) {
        return res.status(400).json({
            message: `Error 400: User Id phai bat buoc!`
        })
    }

    if (!mongoose.Types.ObjectId.isValid(body.user)) {
        return res.status(400).json({
            message: `Error 400: User Id khong dung!`
        })
    }

    if (!body.voucher) {
        return res.status(400).json({
            message: `Error 400: Voucher Id phai bat buoc!`
        })
    }

    if (!mongoose.Types.ObjectId.isValid(body.voucher)) {
        return res.status(400).json({
            message: `Error 400: Voucher Id khong dung!`
        })
    }

    const newVoucherHistory = new voucherHistoryModel({
        _id: mongoose.Types.ObjectId(),
        user: body.user,
        voucher: body.voucher,
    })

    voucherHistoryModel.create(newVoucherHistory, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(201).json({
                message: `Create VoucherHistory successfull!`,
                voucherHistory: data
            })
        }
    })
};

const getAllVoucherHistory = (req, res) => {
    let username = req.query.username;
    if (username) {
        userModel.findOne({ username })
            .exec((error, hasUser) => {
                if (error) {
                    return res.status(500).json({
                        status: `Error 500`,
                        message: `${error.message}`
                    })
                } else {
                    if (hasUser) {
                        let userId = hasUser._id;
                        let condition = {};

                        if (userId) {
                            condition.user = userId;
                        }

                        voucherHistoryModel.find(condition)
                            .exec((error, data) => {
                                if (error) {
                                    return res.status(500).json({
                                        message: `Error 500: ${error.message}`
                                    })
                                } else {
                                    return res.status(200).json({
                                        message: `Load data successfull!`,
                                        voucherHistory: data
                                    })
                                }
                            })
                    } else {
                        return res.status(200).json({
                            message: `No data!`,
                            data: null
                        })
                    };
                };
            });
    } else {
        voucherHistoryModel.find()
            .exec((error, data) => {
                if (error) {
                    return res.status(500).json({
                        message: `Error 500: ${error.message}`
                    })
                } else {
                    return res.status(200).json({
                        message: `Load data successfull!`,
                        voucherHistory: data
                    })
                }
            })
    }

};

const getVoucherHistoryById = (req, res) => {
    let voucherHistoryId = req.params.voucherHistoryId;

    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return res.status(400).json({
            message: `Error 400: Id VoucherHistory khong dung!`
        })
    }

    voucherHistoryModel.findById(voucherHistoryId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(200).json({
                message: `Load data successfull!`,
                voucherHistory: data
            })
        }
    })
};

const updateVoucherHistoryById = (req, res) => {
    let voucherHistoryId = req.params.voucherHistoryId;

    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return res.status(400).json({
            message: `Error 400: Id VoucherHistory khong dung!`
        })
    }

    let body = req.body;

    if (!body.user) {
        return res.status(400).json({
            message: `Error 400: User Id phai bat buoc!`
        })
    }

    if (!mongoose.Types.ObjectId.isValid(body.user)) {
        return res.status(400).json({
            message: `Error 400: User Id khong dung!`
        })
    }

    if (!body.voucher) {
        return res.status(400).json({
            message: `Error 400: Voucher Id phai bat buoc!`
        })
    }

    if (!mongoose.Types.ObjectId.isValid(body.voucher)) {
        return res.status(400).json({
            message: `Error 400: Voucher Id khong dung!`
        })
    }

    const voucherHistory = new voucherHistoryModel({
        user: body.user,
        voucher: body.voucher,
    })

    voucherHistoryModel.findByIdAndUpdate(voucherHistoryId, voucherHistory, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(200).json({
                message: `Update VoucherHistory successfull!`,
                voucherHistory: data
            })
        }
    })
};

const deleteVoucherHistoryById = (req, res) => {
    let voucherHistoryId = req.params.voucherHistoryId;

    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return res.status(400).json({
            message: `Error 400: Id VoucherHistory khong dung!`
        })
    }

    voucherHistoryModel.findByIdAndDelete(voucherHistoryId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(204).json({
                message: `Delete VoucherHistory successfull!`,
                voucherHistory: data
            })
        }
    })
};

module.exports = {
    createVoucherHistory,
    getAllVoucherHistory,
    getVoucherHistoryById,
    updateVoucherHistoryById,
    deleteVoucherHistoryById
}