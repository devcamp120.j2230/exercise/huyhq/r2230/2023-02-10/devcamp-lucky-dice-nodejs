const { default: mongoose } = require("mongoose");
const userModel = require("../model/User");
const voucherModel = require("../model/Voucher");
const diceHistoryModel = require("../model/DiceHistory");
const voucherHistoryModel = require("../model/VoucherHistory");
const prizeHistoryModel = require("../model/PrizeHistory");

const e = require("express");

const getLuckyDice = (req, res) => {
    //random dice
    var dice = Math.floor(Math.random() * 6 + 1);
    let body = req.body;
    if (validateData(body, res)) {
        //kiem tra du lieu userId
        getUserId(body, (userId) => {
            //xử lý dữ liệu đầu vào
            handleData(userId, dice, res);
        });
    }

};

//validate dữ liệu body
const validateData = (body, res) => {
    let firstname = body.firstname;
    let lastname = body.lastname;
    let username = body.username;

    if (!firstname) {
        return res.status(400).json({
            status: "Error 400",
            message: "Firstname is required!"
        })
    };

    if (!lastname) {
        return res.status(400).json({
            status: "Error 400",
            message: "Lastname is required!"
        })
    };

    if (!username) {
        return res.status(400).json({
            status: "Error 400",
            message: "Username is required!"
        })
    };

    return true;
};
//tạo mới User
const createNewUser = (body) => {
    const newUser = userModel({
        _id: mongoose.Types.ObjectId(),
        username: body.username,
        firstname: body.firstname,
        lastname: body.lastname,
    });

    userModel.create(newUser, (error, data) => {
        if (error) {
            console.error(error.message);
        } else {
            return data._id;
        }
    })
};
//tạo mới DiceHistory
const createNewDiceHistory = (userId, dice) => {
    const newDice = new diceHistoryModel({
        _id: mongoose.Types.ObjectId(),
        user: userId,
        dice: dice,
    })

    diceHistoryModel.create(newDice, (error, data) => {
        if (error) {
            console.error(error.message);
        }
        // else {
        //     return res.status(201).json({
        //         message: `Create Dice successfull!`,
        //         dice: data
        //     })
        // }
    })
};
//lấy random voucher
const getRandVoucher = (userId, dice, callback) => {
    if (dice > 3) {
        //đếm số bản ghi dữ liệu trong model
        voucherModel.count().exec(async (error, count) => {
            if (error) {
                console.error(error.message);
            } else {
                //tạo 1 số bất kỳ từ 0 đến số bản ghi tối đa
                //randrom để bỏ qua số đã chọn, lấy 1 bản ghi tiếp theo
                let rand = Math.floor(Math.random() * count);
                voucherModel.findOne()
                    .skip(rand)
                    .exec((error, data) => {
                        if (error) {
                            console.error(error.message);
                        } else {
                            let voucherId = data._id;
                            createVoucherHistory(userId, voucherId);
                            callback(data)
                            // return data;
                        }
                    })
            }
        })
    } else {
        callback(null);
        // return null;
    }
};
//tạo mới Voucher History
const createVoucherHistory = (userId, voucherId) => {
    const newVoucherHistory = new voucherHistoryModel({
        _id: mongoose.Types.ObjectId(),
        user: userId,
        voucher: voucherId,
    })

    voucherHistoryModel.create(newVoucherHistory, (error, data) => {
        if (error) {
            console.error(error.message);
        } else {
            return data;
        }

        // else {
        //     return res.status(201).json({
        //         message: `Create VoucherHistory successfull!`,
        //         voucherHistory: data
        //     })
        // }
    })


};
//lấy random prize
const getRandomPrize = (userId, callback) => {
    //lấy rando//đếm số bản ghi dữ liệu trong model
    prizeHistoryModel.count().exec((error, count) => {
        if (error) {
            console.error(error.message);
        } else {
            //tạo 1 số bất kỳ từ 0 đến số bản ghi tối đa
            //randrom để bỏ qua số đã chọn, lấy 1 bản ghi tiếp theo
            let rand = Math.floor(Math.random() * count);
            prizeHistoryModel.findOne()
                .skip(rand)
                .exec((error, data) => {
                    if (error) {
                        console.error(error.message);
                    } else {
                        let prizeId = data._id;
                        createNewPrizeHistory(userId, prizeId)
                        callback(data);
                        // return data;
                    }

                })
        }

    })
}
//tạo mới PrizeHistory
const createNewPrizeHistory = (userId, prizeId) => {

    //tạo mới prize history
    const newPrizeHistory = new prizeHistoryModel({
        _id: mongoose.Types.ObjectId(),
        user: userId,
        prize: prizeId,
    })

    prizeHistoryModel.create(newPrizeHistory, (error, data) => {
        if (error) {
            console.error(error.message);
        } else {
            return data;
        }

        // else {
        //     return res.status(201).json({
        //         message: `Create PrizeHistory successfull!`,
        //         prizeHistory: data
        //     })
        // }
    })
};

//kiểm tra username
const getUserId = async function (body, callback) {
    var username = body.username;
    var userId;
    userModel.findOne({username})
        .exec((error, data) => {
            if (error) {
                console.error(error.message);
            } else {
                if (data) {
                    userId = data._id;
                    callback(userId);
                } else {
                    userId = createNewUser(body);
                    callback(userId);
                }
            };
        });
};

//xử lý dữ liệu
const handleData = (userId, dice, res) => {
    console.log("Dice: " + dice);
    //thêm dữ liệu Dice History
    createNewDiceHistory(userId, dice);
    var prize = null;
    var voucher = null;
    //lấy ra 2 bản ghi Dice History cuối (sau bản ghi mới tạo)
    diceHistoryModel.find({ user: userId })
        .sort({ createdAt: "descending" })
        .limit(2)
        .exec((error, data) => {
            if (error) {
                console.error(error.message);
            } else {
                //kiểm tra giá trị dice
                let check = dice <= 3 ? false : true;

                for (let i = 0; i < data.length; i++) {
                    if (data[i].dice <= 3) check = false;
                }

                if (check === true) {
                    getRandomPrize(userId, (prizeData) => {
                        getRandVoucher(userId, dice, (voucherData) => {
                            voucher = voucherData;
                            prize = prizeData;
                            returnApiData(res, userId, dice, voucher, prize);
                        })
                    })
                } else {
                    getRandVoucher(userId, dice, (voucherData) => {
                        voucher = voucherData;
                        prize = null;
                        returnApiData(res, userId, dice, voucher, prize);
                    })
                };
            }
        })
}
//return API
const returnApiData = (res, userId, dice, voucher, prize) => {
    return res.status(200).json({
        status: "Get data successfull!",
        data: {
            user: userId,
            dice,
            voucher,
            prize
        }
    });
};


module.exports = {
    getLuckyDice
}