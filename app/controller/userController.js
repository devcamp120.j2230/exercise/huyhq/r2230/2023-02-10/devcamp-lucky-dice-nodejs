const { default: mongoose } = require("mongoose");

const userModel = require("../model/User");

const createUser = (req, res) => {
    let dice = Math.floor(Math.random()*6+1);

    let body = req.body;
    if (!body.username) {
        return res.status(400).json({
            message: `Error 400: Username phai bat buoc!`
        })
    }

    if (!body.firstname) {
        return res.status(400).json({
            message: `Error 400: Firstname phai bat buoc!`
        })
    }

    if (!body.lastname) {
        return res.status(400).json({
            message: `Error 400: Lastname phai bat buoc!`
        })
    }
    const newUser = new userModel({
        _id: mongoose.Types.ObjectId(),
        username: body.username,
        firstname: body.firstname,
        lastname: body.lastname,
    })

    userModel.create(newUser, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(201).json({
                message: `Create User successfull!`,
                user: data
            })
        }
    })
};

const getAllUser = (req, res) => {
    userModel.find((error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(200).json({
                message: `Load data successfull!`,
                user: data
            })
        }
    })
};

const getUserById = (req, res) => {
    let userId = req.params.userId;

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: `Error 400: Id User khong dung!`
        })
    }

    userModel.findById(userId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(200).json({
                message: `Load data successfull!`,
                user: data
            })
        }
    })
};
const updateUserById = (req, res) => {
    let userId = req.params.userId;

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: `Error 400: Id User khong dung!`
        })
    }

    let body = req.body;
    if (!body.username) {
        return res.status(400).json({
            message: `Error 400: Username phai bat buoc!`
        })
    }

    if (!body.firstname) {
        return res.status(400).json({
            message: `Error 400: Firstname phai bat buoc!`
        })
    }

    if (!body.firstname) {
        return res.status(400).json({
            message: `Error 400: Lasttname phai bat buoc!`
        })
    }
    const user = new userModel({
        username: body.username,
        firstname: body.firstname,
        lastname: body.lastname,
    })

    userModel.findByIdAndUpdate(userId, user, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(200).json({
                message: `Update User successfull!`,
                user: data
            })
        }
    })
};
const deleteUserById = (req, res) => {
    let userId = req.params.userId;

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: `Error 400: Id User khong dung!`
        })
    }

    userModel.findByIdAndDelete(userId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(204).json({
                message: `Delete User successfull!`,
                user: data
            })
        }
    })
};

module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById
}