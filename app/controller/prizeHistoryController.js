const { default: mongoose } = require("mongoose");

const prizeHistoryModel = require("../model/PrizeHistory");
const userModel = require("../model/User");

const createPrizeHistory = (req, res) => {
    let body = req.body;

    if (!body.user) {
        return res.status(400).json({
            message: `Error 400: User Id phai bat buoc!`
        })
    }

    if (!mongoose.Types.ObjectId.isValid(body.user)) {
        return res.status(400).json({
            message: `Error 400: User Id khong dung!`
        })
    }

    if (!body.prize) {
        return res.status(400).json({
            message: `Error 400: Prize Id phai bat buoc!`
        })
    }

    if (!mongoose.Types.ObjectId.isValid(body.prize)) {
        return res.status(400).json({
            message: `Error 400: Prize Id khong dung!`
        })
    }

    const newPrizeHistory = new prizeHistoryModel({
        _id: mongoose.Types.ObjectId(),
        user: body.user,
        prize: body.prize,
    })

    prizeHistoryModel.create(newPrizeHistory, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(201).json({
                message: `Create PrizeHistory successfull!`,
                prizeHistory: data
            })
        }
    })
};

const getAllPrizeHistory = (req, res) => {
    let username = req.query.username;
    if (username) {
        userModel.findOne({ username })
            .exec((error, hasUser) => {
                if (error) {
                    return res.status(500).json({
                        status: `Error 500`,
                        message: `${error.message}`
                    })
                } else {
                    if (hasUser) {
                        let userId = hasUser._id;
                        let condition = {};

                        if (userId) {
                            condition.user = userId;
                        }

                        prizeHistoryModel.find(condition)
                            .exec((error, data) => {
                                if (error) {
                                    return res.status(500).json({
                                        message: `Error 500: ${error.message}`
                                    })
                                } else {
                                    return res.status(200).json({
                                        message: `Load data successfull!`,
                                        prizeHistory: data
                                    })
                                }
                            })
                    } else {
                        return res.status(200).json({
                            message: `No data!`,
                            data: null
                        })
                    }
                };
            });
    } else {
        prizeHistoryModel.find()
            .exec((error, data) => {
                if (error) {
                    return res.status(500).json({
                        message: `Error 500: ${error.message}`
                    })
                } else {
                    return res.status(200).json({
                        message: `Load data successfull!`,
                        prizeHistory: data
                    })
                }
            })
    }

};

const getPrizeHistoryById = (req, res) => {
    let prizeHistoryId = req.params.prizeHistoryId;

    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return res.status(400).json({
            message: `Error 400: Id PrizeHistory khong dung!`
        })
    }

    prizeHistoryModel.findById(prizeHistoryId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(200).json({
                message: `Load data successfull!`,
                prizeHistory: data
            })
        }
    })
};

const updatePrizeHistoryById = (req, res) => {
    let prizeHistoryId = req.params.prizeHistoryId;

    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return res.status(400).json({
            message: `Error 400: Id PrizeHistory khong dung!`
        })
    }

    let body = req.body;

    if (!body.user) {
        return res.status(400).json({
            message: `Error 400: User Id phai bat buoc!`
        })
    }

    if (!mongoose.Types.ObjectId.isValid(body.user)) {
        return res.status(400).json({
            message: `Error 400: User Id khong dung!`
        })
    }

    if (!body.prize) {
        return res.status(400).json({
            message: `Error 400: Prize Id phai bat buoc!`
        })
    }

    if (!mongoose.Types.ObjectId.isValid(body.prize)) {
        return res.status(400).json({
            message: `Error 400: Prize Id khong dung!`
        })
    }

    const prizeHistory = new prizeHistoryModel({
        user: body.user,
        prize: body.prize,
    })

    prizeHistoryModel.findByIdAndUpdate(prizeHistoryId, prizeHistory, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(200).json({
                message: `Update PrizeHistory successfull!`,
                prizeHistory: data
            })
        }
    })
};

const deletePrizeHistoryById = (req, res) => {
    let prizeHistoryId = req.params.prizeHistoryId;

    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return res.status(400).json({
            message: `Error 400: Id PrizeHistory khong dung!`
        })
    }

    prizeHistoryModel.findByIdAndDelete(prizeHistoryId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(204).json({
                message: `Delete PrizeHistory successfull!`,
                prizeHistory: data
            })
        }
    })
};

module.exports = {
    createPrizeHistory,
    getAllPrizeHistory,
    getPrizeHistoryById,
    updatePrizeHistoryById,
    deletePrizeHistoryById
}