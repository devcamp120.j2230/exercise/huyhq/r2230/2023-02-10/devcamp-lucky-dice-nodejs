const { default: mongoose } = require("mongoose");

const diceHistoryModel = require("../model/DiceHistory");
const userModel = require("../model/User");

const createDiceHistory = (req, res) => {
    let body = req.body;
    if (!body.user) {
        return res.status(400).json({
            message: `Error 400: User Id phai bat buoc!`
        })
    }

    if (!mongoose.Types.ObjectId.isValid(body.user)) {
        return res.status(400).json({
            message: `Error 400: Id User khong dung!`
        })
    }

    if (!body.dice) {
        return res.status(400).json({
            message: `Error 400: Dice number phai bat buoc!`
        })
    }

    if (!Number.isInteger(body.dice) || body.dice < 0 || body.dice > 6) {
        return res.status(400).json({
            message: `Error 400: Dice number khong dung!`
        })
    }

    const newDice = new diceHistoryModel({
        _id: mongoose.Types.ObjectId(),
        user: body.user,
        dice: body.dice,
    })

    diceHistoryModel.create(newDice, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(201).json({
                message: `Create Dice successfull!`,
                data: data
            })
        }
    })
};

const getAllDiceHistory = (req, res) => {
    let username = req.query.username;
    if (username) {
        userModel.findOne({ username: username })
            .exec((error, hasUser) => {
                if (error) {
                    return res.status(500).json({
                        status: `Error 500`,
                        message: `${error.message}`
                    })
                } else {
                    if (hasUser) {
                        let userId = hasUser._id;
                        let condition = {};

                        if (userId) {
                            condition.user = userId
                        };

                        diceHistoryModel.find(condition)
                            .exec((error, data) => {
                                if (error) {
                                    return res.status(500).json({
                                        message: `Error 500: ${error.message}`
                                    })
                                } else {
                                    return res.status(200).json({
                                        message: `Load data successfull!`,
                                        data: data
                                    })
                                }
                            })
                    } else {
                        return res.status(200).json({
                            message: `No data!`,
                            data: null
                        })
                    }
                }
            });
    } else {
        diceHistoryModel.find()
            .exec((error, data) => {
                if (error) {
                    return res.status(500).json({
                        message: `Error 500: ${error.message}`
                    })
                } else {
                    return res.status(200).json({
                        message: `Load data successfull!`,
                        data: data
                    })
                }
            })
    }

};

const getDiceHistoryById = (req, res) => {
    let diceId = req.params.diceId;

    if (!mongoose.Types.ObjectId.isValid(diceId)) {
        return res.status(400).json({
            message: `Error 400: Id Dice khong dung!`
        })
    }

    diceHistoryModel.findById(diceId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(200).json({
                message: `Load data successfull!`,
                data: data
            })
        }
    })
};
const updateDiceHistoryById = (req, res) => {
    let diceId = req.params.diceId;

    if (!mongoose.Types.ObjectId.isValid(diceId)) {
        return res.status(400).json({
            message: `Error 400: Id Dice khong dung!`
        })
    }

    let body = req.body;
    if (!body.user) {
        return res.status(400).json({
            message: `Error 400: User Id phai bat buoc!`
        })
    }

    if (!mongoose.Types.ObjectId.isValid(body.user)) {
        return res.status(400).json({
            message: `Error 400: Id User khong dung!`
        })
    }

    if (!body.dice) {
        return res.status(400).json({
            message: `Error 400: Dice number phai bat buoc!`
        })
    }

    if (!Number.isInteger(body.dice) || body.dice < 0 || body.dice > 6) {
        return res.status(400).json({
            message: `Error 400: Dice number khong dung!`
        })
    }

    const dice = new diceHistoryModel({
        user: body.user,
        dice: body.dice
    })

    diceHistoryModel.findByIdAndUpdate(diceId, dice, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(200).json({
                message: `Update Dice successfull!`,
                data: data
            })
        }
    })
};
const deleteDiceHistoryById = (req, res) => {
    let diceId = req.params.diceId;

    if (!mongoose.Types.ObjectId.isValid(diceId)) {
        return res.status(400).json({
            message: `Error 400: Id Dice khong dung!`
        })
    }

    diceHistoryModel.findByIdAndDelete(diceId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(204).json({
                message: `Delete Dice successfull!`,
                data: data
            })
        }
    })
};

module.exports = {
    createDiceHistory,
    getAllDiceHistory,
    getDiceHistoryById,
    updateDiceHistoryById,
    deleteDiceHistoryById
}