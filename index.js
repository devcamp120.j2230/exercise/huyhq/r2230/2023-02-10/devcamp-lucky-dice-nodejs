const express = require("express");
const mongoose = require('mongoose');

const path = require("path");

const app = express();
app.use(express.json())

const port = 8000;

const dbName = "CRUD_LucyDice";
mongoose.connect("mongodb://localhost:27017/"+dbName, (error)=>{
    if(error) throw error;
    else{
        console.log("Successfull connect to: "+dbName);
    }
})
//Import model
const {userModel} = require("./app/model/User");
const {diceHistoryModel} = require("./app/model/DiceHistory");
const {prizeModel} = require("./app/model/Prize");
const {voucherModel} = require("./app/model/Voucher");
const {prizeHistory} = require("./app/model/PrizeHistory");
const {voucherHistory} = require("./app/model/VoucherHistory");
//Import routes
const {userRouter} = require("./app/routes/userRoute");
const {diceHistoryRouter} = require("./app/routes/diceHistoryRoute");
const {prizeRouter} = require("./app/routes/prizeRoute");
const {voucherRouter} = require("./app/routes/voucherRoute");
const {prizeHistoryRouter} = require("./app/routes/prizeHistoryRoute");
const {voucherHistoryRouter} = require("./app/routes/voucherHistoryRoute");
const {luckDiceRouter} = require("./app/routes/luckyDiceRoute");

app.use(express.static(__dirname + "/view"));

app.use("/", (req, res, next) => {
    console.log("Date: " + new Date());
    next();
});

app.use("/", (req, res, next) => {
    console.log("Method: " + req.method);
    next();
});

app.get("/", (req, res) => {
    // var rand = Math.floor(Math.random()*6+1);
    // return res.status(200).json({
    //     rand
    // });

    return res.sendFile(path.join(__dirname + "/view/Lucky Dice Casino.html"))

});

app.use("/devcamp-lucky-dice/", userRouter);
app.use("/devcamp-lucky-dice/", diceHistoryRouter);
app.use("/devcamp-lucky-dice/", prizeRouter);
app.use("/devcamp-lucky-dice/", voucherRouter);
app.use("/devcamp-lucky-dice/", prizeHistoryRouter);
app.use("/devcamp-lucky-dice/", voucherHistoryRouter);
app.use("/devcamp-lucky-dice/", luckDiceRouter);

app.listen(port, () => {
    console.log("Connect to port: " + port);
})